# Prototyp für DTS+ mit MS Prism5

## Projektmappen-Struktur

- ROOT
    - Modules
        - Module1
        - Module2
        - ...
    - UnitTests
        - Infrastructure.Tests
        - Shell.Tests
        - Module1.Tests
        - Module2.Test
        - ...
    - Shell
    - Infrastructure

### Infrastructure
Projekt mit gemeinsam genutzten Resourcen:

- Interfaces
- Basisimplementierungen der Interfaces
- Services
- Globale Commands
- Event Aggregator
- usw.

Dieses Projekt kann in allen Modulen referenziert werden.

### Sehll
Projekt, in dem die Module geladen werden (enkoppelt) und in die Anzeige (Shell) eingebunden werden. Hier werden auch die gemeinsam genutzen Resourcen angemeldet:

- DI Container
- Logger
- Dialog-Services
- Event Aggregatoren
- usw.

### Modules
In diesem Ordner werden die einzelnen Prism5 Module hinterlegt. Die DLLs werden dabei in den `Modules` Ordner kopiert oder direkt gebuildet. Zum direkten Builden in den gemeinsamen Ausgabeverzeichnis kann in der Projektdatei folgendes als `<OutputPath>` verwendet werden:

- `$(SolutionDir)BuildOutput\$(Configuration)\Modules\`

### UnitTests
In diesem ordner werden die UnitTest zu der Prism-Basis und den einzelnen Modulen abgelegt. Zu jedem Modul soll es mindestens einen Testprojekt geben.

## Gemeinsame Resourcen

### Interfaces

#### `IClassLoggerService`
Interface für Service, der einen Logger mit dem Interface `IClassLogger` zurück liefern kann. Momentan ist nur eine Implementierung für NLog verfügbar.

#### `IClassLogger`
Allgemeiner Interface für den Zugriff auf die Logger.

### Events

#### `StatusbarStateChangedEvent`
Event Payload Klasse für den Eventagregator. In der Demo wird es dazu genutzt, um einen Teil der Statusbar mit Statusinformationen aus den unterschiedlichen Modulen zu versorgen.

### Konstanten

#### `RegionNames`
Namen der Regionen in der Haupt-Shell.

## Erstellung eines Modules
Prism arbeitet nur mit WPF-Anwendungen zusammen. Alle Module, die etwas visuelles liefern, müssen in XAML designt sein (im optimalen Fall auch nach MVVM).

Die Einstiegsklasse für das Modul muss das Interface `IModule` implementieren. Die Basisklasse `BaseModel` implementiert diese und initialisiert bereits folgende oft benutzte Dienste:

- Logger : `ICLassLogger`  
    _Zum Loggen mit den Klassennamen als Loggger-ID_
- RegionManager : `IRegionManager`  
    _Zum Zugriff auf den RegionManager (Einbinden der Views in der Shell)_
- Container : `IDiContainer`  
    _Zum Zugriff auf den benutzten Container für das Auflösen der Klassen oder für deren Registrierung_
    
### Navigation
In der `Initialize` Methode des Moduls können bereits bestimmte Views geladen werden, die immer angezeigt werden sollten (z.B. die Menüpunkte im Menü oder in der Toolbar, über die andere Views dynamisch geladen werden sollen).

````csharp
RegionManager.RegisterViewWithRegion(RegionNames.CONTENT_REGION,
    typeof (ContentView));
````

Wenn ein View dynamisch geladen werden soll (z.B. über ein Button oder Menüeintrag), muss dieses View im Container mit einen eindeutigen Namen registriert werden.

````csharp
Container.RegisterType<object, ContentView>(
    typeof(ContentView).FullName);
````

Nach der Registrierung kann zu diesem View jederzeit navigiert werden.

````csharp
regionManager.RequestNavigate(RegionNames.CONTENT_REGION,
    new Uri(typeof (ContentView).FullName, UriKind.Relative));
````

Bei der Navigation können auch Paramtere mit übergeben werden. Dazu muss das View oder das zugeordnete ViewModel das Interface `INavigationAware` implementieren. Durch dieses kann auf die Navigationsanforderung reagiert werden.

### Kommunikation

Kommunikation zwischen den Modulen läuft entweder über die Parameter in der Navigation, oder über die Registrierung beim Event-Aggregator zum Empfang / Versand.

````csharp
Container.GetEvent<StatusbarStateChangedEvent>()
    .Subscribe(s => StatusMessage = s);
````

````csharp
Container.GetEvent<StatusbarStateChangedEvent>()
    .Publish(@"View from 'NLogOutputModule' loaded!")
````

Der verwendete EventAggregator unterstützt multiple Empfänger und Sender für den selben Nachrichtentyp.