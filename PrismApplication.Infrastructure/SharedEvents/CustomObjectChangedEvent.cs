﻿using de.webducer.dotnet.PrismApplication.Infrastructure.Shared_Objects;
using Microsoft.Practices.Prism.PubSubEvents;

namespace de.webducer.dotnet.PrismApplication.Infrastructure.SharedEvents
{
   public class CustomObjectChangedEvent : PubSubEvent<MySharedObject>
   {

   }
}