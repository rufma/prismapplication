﻿using System;

namespace de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces {
   public interface IClassLoggerService {
      IClassLogger GetClassLogger<T>();

      IClassLogger GetClassLogger(Type type);
   }
}