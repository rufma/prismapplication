﻿using System;

namespace de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces {
   public interface IClassLogger {

      #region Trace

      void Trace(string message);

      void Trace(string formatString, params object[] arguments);

      void Trace(string message, Exception exeption);

      void Trace<T>(T value);

      void Trace<TArgument>(string formatString, TArgument argument);

      void Trace<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2);

      #endregion

      #region Debug

      void Debug(string message);

      void Debug(string formatString, params object[] arguments);

      void Debug(string message, Exception exeption);

      void Debug<T>(T value);

      void Debug<TArgument>(string formatString, TArgument argument);

      void Debug<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2);

      #endregion

      #region Info

      void Info(string message);

      void Info(string formatString, params object[] arguments);

      void Info(string message, Exception exeption);

      void Info<T>(T value);

      void Info<TArgument>(string formatString, TArgument argument);

      void Info<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2);

      #endregion

      #region Warn

      void Warn(string message);

      void Warn(string formatString, params object[] arguments);

      void Warn(string message, Exception exeption);

      void Warn<T>(T value);

      void Warn<TArgument>(string formatString, TArgument argument);

      void Warn<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2);

      #endregion

      #region Error

      void Error(string message);

      void Error(string formatString, params object[] arguments);

      void Error(string message, Exception exeption);

      void Error<T>(T value);

      void Error<TArgument>(string formatString, TArgument argument);

      void Error<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2);

      #endregion

      #region Fatal

      void Fatal(string message);

      void Fatal(string formatString, params object[] arguments);

      void Fatal(string message, Exception exeption);

      void Fatal<T>(T value);

      void Fatal<TArgument>(string formatString, TArgument argument);

      void Fatal<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2);

      #endregion
   }
}