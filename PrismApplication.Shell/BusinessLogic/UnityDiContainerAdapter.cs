﻿using System;
using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;

namespace de.webducer.dotnet.PrismApplication.Shell.BusinessLogic {
   public class UnityDiContainerAdapter : UnityServiceLocatorAdapter, IDiContainer {
      private readonly IUnityContainer _unityContainer;

      public UnityDiContainerAdapter(IUnityContainer unityContainer) : base(unityContainer) {
         _unityContainer = unityContainer;
      }

      #region Instance Registration

      public IDiContainer RegisterInstance(Type t, string name, object instance, LifeType lifeType) {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterInstance(t, name, instance, new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterInstance(t, name, instance, new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterInstance(t, name, instance);
               break;
         }

         return this;
      }

      public IDiContainer RegisterInstance(Type t, object instance, LifeType lifeType = LifeType.Default) {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterInstance(t, instance, new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterInstance(t, instance, new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterInstance(t, instance);
               break;
         }

         return this;
      }

      public IDiContainer RegisterInstance<TInterface>(string name, TInterface instance, LifeType lifeType = LifeType.Default) {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterInstance<TInterface>(name, instance, new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterInstance<TInterface>(name, instance, new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterInstance<TInterface>(name, instance);
               break;
         }

         return this;
      }

      public IDiContainer RegisterInstance<TInterface>(TInterface instance, LifeType lifeType = LifeType.Default) {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterInstance<TInterface>(instance, new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterInstance<TInterface>(instance, new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterInstance<TInterface>(instance);
               break;
         }

         return this;
      }

      #endregion

      #region Register Type

      public IDiContainer RegisterType(Type fromType, Type toType, string name, LifeType lifeType) {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterType(fromType, toType, name, new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterType(fromType, toType, name, new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterType(fromType, toType, name);
               break;
         }

         return this;
      }

      public IDiContainer RegisterType(Type fromType, Type toType, LifeType lifeType = LifeType.Default) {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterType(fromType, toType, new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterType(fromType, toType, new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterType(fromType, toType);
               break;
         }

         return this;
      }

      public IDiContainer RegisterType<T>(string name, LifeType lifeType = LifeType.Default) {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterType<T>(name, new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterType<T>(name, new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterType<T>(name);
               break;
         }

         return this;
      }

      public IDiContainer RegisterType<T>(LifeType lifeType = LifeType.Default) {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterType<T>(new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterType<T>(new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterType<T>();
               break;
         }

         return this;
      }

      public IDiContainer RegisterType<TFrom, TTo>(string name, LifeType lifeType = LifeType.Default) where TTo : TFrom {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterType<TFrom, TTo>(name, new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterType<TFrom, TTo>(name, new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterType<TFrom, TTo>(name);
               break;
         }

         return this;
      }

      public IDiContainer RegisterType<TFrom, TTo>(LifeType lifeType = LifeType.Default) where TTo : TFrom {
         switch (lifeType) {
            case LifeType.Singleton:
               _unityContainer.RegisterType<TFrom, TTo>(new ContainerControlledLifetimeManager());
               break;

            case LifeType.PerThread:
               _unityContainer.RegisterType<TFrom, TTo>(new PerThreadLifetimeManager());
               break;

            default:
               _unityContainer.RegisterType<TFrom, TTo>();
               break;
         }

         return this;
      }

      #endregion

      #region Get Event

      public T GetEvent<T>() where T : EventBase, new() {
         return _unityContainer.Resolve<IEventAggregator>().GetEvent<T>();
      }

      #endregion
   }
}