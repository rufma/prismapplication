﻿using System;
using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;
using NLog;

namespace de.webducer.dotnet.PrismApplication.Shell.BusinessLogic {
   public class NLogClassLogger : IClassLogger {
      #region Constants
      private const string _DEFAULT_LOGGER_NAME = "Default";
      private readonly Logger _logger;
      #endregion

      #region Constructors
      public NLogClassLogger(Type typeOfClass) {
         _logger = typeOfClass == null ? LogManager.GetLogger(_DEFAULT_LOGGER_NAME) : LogManager.GetLogger(typeOfClass.FullName);
      }
      #endregion

      #region IClassLogger Interface

      #region Trace Methods
      public void Trace(string message) {
         if (_logger != null) {
            _logger.Trace(message);
         }
      }

      public void Trace(string formatString, params object[] arguments) {
         if (_logger != null) {
            _logger.Trace(formatString, arguments);
         }
      }

      public void Trace(string message, Exception exeption) {
         if(_logger != null) {
            _logger.Trace(message, exeption);
         }
      }

      public void Trace<T>(T value) {
         if(_logger != null) {
            _logger.Trace(value);
         }
      }

      public void Trace<TArgumnet>(string formatString, TArgumnet argument) {
         if(_logger != null) {
            _logger.Trace(formatString, argument);
         }
      }

      public void Trace<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2) {
         if(_logger != null) {
            _logger.Trace(formatString, argument1, argument2);
         }
      }
      #endregion

      #region Debug Methods
      public void Debug(string message) {
         if(_logger != null) {
            _logger.Debug(message);
         }
      }

      public void Debug(string formatString, params object[] arguments) {
         if(_logger != null) {
            _logger.Debug(formatString, arguments);
         }
      }

      public void Debug(string message, Exception exeption) {
         if(_logger != null) {
            _logger.Debug(message, exeption);
         }
      }

      public void Debug<T>(T value) {
         if(_logger != null) {
            _logger.Debug(value);
         }
      }

      public void Debug<TArgumnet>(string formatString, TArgumnet argument) {
         if(_logger != null) {
            _logger.Debug(formatString, argument);
         }
      }

      public void Debug<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2) {
         if(_logger != null) {
            _logger.Debug(formatString, argument1, argument2);
         }
      }
      #endregion

      #region Info Methods
      public void Info(string message) {
         if(_logger != null) {
            _logger.Info(message);
         }
      }

      public void Info(string formatString, params object[] arguments) {
         if(_logger != null) {
            _logger.Info(formatString, arguments);
         }
      }

      public void Info(string message, Exception exeption) {
         if(_logger != null) {
            _logger.Info(message, exeption);
         }
      }

      public void Info<T>(T value) {
         if(_logger != null) {
            _logger.Info(value);
         }
      }

      public void Info<TArgumnet>(string formatString, TArgumnet argument) {
         if(_logger != null) {
            _logger.Info(formatString, argument);
         }
      }

      public void Info<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2) {
         if(_logger != null) {
            _logger.Info(formatString, argument1, argument2);
         }
      }
      #endregion

      #region Warn Methods
      public void Warn(string message) {
         if(_logger != null) {
            _logger.Warn(message);
         }
      }

      public void Warn(string formatString, params object[] arguments) {
         if(_logger != null) {
            _logger.Warn(formatString, arguments);
         }
      }

      public void Warn(string message, Exception exeption) {
         if(_logger != null) {
            _logger.Warn(message, exeption);
         }
      }

      public void Warn<T>(T value) {
         if(_logger != null) {
            _logger.Warn(value);
         }
      }

      public void Warn<TArgumnet>(string formatString, TArgumnet argument) {
         if(_logger != null) {
            _logger.Warn(formatString, argument);
         }
      }

      public void Warn<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2) {
         if(_logger != null) {
            _logger.Warn(formatString, argument1, argument2);
         }
      }
      #endregion

      #region Error Methods
      public void Error(string message) {
         if(_logger != null) {
            _logger.Error(message);
         }
      }

      public void Error(string formatString, params object[] arguments) {
         if(_logger != null) {
            _logger.Error(formatString, arguments);
         }
      }

      public void Error(string message, Exception exeption) {
         if(_logger != null) {
            _logger.Error(message, exeption);
         }
      }

      public void Error<T>(T value) {
         if(_logger != null) {
            _logger.Error(value);
         }
      }

      public void Error<TArgumnet>(string formatString, TArgumnet argument) {
         if(_logger != null) {
            _logger.Error(formatString, argument);
         }
      }

      public void Error<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2) {
         if(_logger != null) {
            _logger.Error(formatString, argument1, argument2);
         }
      }
      #endregion

      #region Fatal Methods
      public void Fatal(string message) {
         if(_logger != null) {
            _logger.Fatal(message);
         }
      }

      public void Fatal(string formatString, params object[] arguments) {
         if(_logger != null) {
            _logger.Fatal(formatString, arguments);
         }
      }

      public void Fatal(string message, Exception exeption) {
         if(_logger != null) {
            _logger.Fatal(message, exeption);
         }
      }

      public void Fatal<T>(T value) {
         if(_logger != null) {
            _logger.Fatal(value);
         }
      }

      public void Fatal<TArgumnet>(string formatString, TArgumnet argument) {
         if(_logger != null) {
            _logger.Fatal(formatString, argument);
         }
      }

      public void Fatal<TArgument1, TArgument2>(string formatString, TArgument1 argument1, TArgument2 argument2) {
         if(_logger != null) {
            _logger.Fatal(formatString, argument1, argument2);
         }
      }
      #endregion
      #endregion
   }
}