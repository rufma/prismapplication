﻿using System;
using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;

namespace de.webducer.dotnet.PrismApplication.Shell.BusinessLogic {
   public class NLogClassLoggerService : IClassLoggerService {
      public IClassLogger GetClassLogger<T>() {
         return GetClassLogger(typeof (T));
      }

      public IClassLogger GetClassLogger(Type type) {
         return new NLogClassLogger(type);
      }
   }
}