﻿using System.Windows.Input;
using de.webducer.dotnet.PrismApplication.Infrastructure.Base;
using de.webducer.dotnet.PrismApplication.MenuStartableModule.Commands;
using Microsoft.Practices.Prism.Regions;

namespace de.webducer.dotnet.PrismApplication.MenuStartableModule.ViewModels
{
   public class ContentViewModel : BaseViewModel, INavigationAware, IRegionMemberLifetime
   {
      #region Constructor

      public ContentViewModel()
      {
         _counter++;
      }

      #endregion

      #region Counter

      private static int _counter;

      /// <summary>
      ///    The <see cref="Counter" /> property's name.
      /// </summary>
      public const string CounterPropertyName = "Counter";

      /// <summary>
      ///    Sets and gets the Counter property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public int Counter
      {
         get { return _counter; }
      }

      #endregion

      #region DoNavigationCommand

      /// <summary>
      ///    The <see cref="DoNavigationCommand" /> property's name.
      /// </summary>
      public const string DoNavigationCommandPropertyName = "DoNavigationCommand";

      private ICommand _doNavigationCommand = new DoNavigationCommand();
      private bool _keepAlive;

      /// <summary>
      ///    Sets and gets the DoNavigationCommand property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public ICommand DoNavigationCommand
      {
         get { return _doNavigationCommand; }
         set { Set(() => DoNavigationCommand, ref _doNavigationCommand, value); }
      }

      #endregion

      #region Implements INavigationAware

      public void OnNavigatedTo(NavigationContext navigationContext)
      {
         Logger.Info("Counter: " + Counter);
      }

      public bool IsNavigationTarget(NavigationContext navigationContext)
      {
         return false;
      }

      public void OnNavigatedFrom(NavigationContext navigationContext)
      {
      }

      #endregion

      #region Implements IRegionMemberLifetime

      public bool KeepAlive
      {
         get { return false; }
      }

      #endregion
   }
}