﻿using de.webducer.dotnet.PrismApplication.Infrastructure.Base;
using de.webducer.dotnet.PrismApplication.Infrastructure.Constants;
using de.webducer.dotnet.PrismApplication.MenuStartableModule.Views;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;

namespace de.webducer.dotnet.PrismApplication.MenuStartableModule {
   [Module(ModuleName = "MenuStartable", OnDemand = false)]
   public class MenuStartableModule : BaseModule {
      public override void Initialize() {
         Logger.Debug(@"Enty: {0}", @"Initialize");

         RegionManager.RegisterViewWithRegion(RegionNames.TOOLBAR_REGION, typeof (MenuView));

         Container.RegisterType<object, ContentView>(typeof (ContentView).FullName);

         Logger.Debug(@"Exit: {0}", @"Initialize");
      }
   }
}