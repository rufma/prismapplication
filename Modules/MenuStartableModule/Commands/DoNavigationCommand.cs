﻿using System;
using System.Windows.Input;
using de.webducer.dotnet.PrismApplication.ClickableButtons.ViewModels;
using de.webducer.dotnet.PrismApplication.ClickableButtons.Views;
using de.webducer.dotnet.PrismApplication.Infrastructure.Constants;
using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;
using de.webducer.dotnet.PrismApplication.Infrastructure.SharedEvents;
using de.webducer.dotnet.PrismApplication.Infrastructure.Shared_Objects;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace de.webducer.dotnet.PrismApplication.MenuStartableModule.Commands
{
   public class DoNavigationCommand : ICommand
   {
      public bool CanExecute(object parameter)
      {
         return true;
      }

      public void Execute(object parameter)
      {
         var container = ServiceLocator.Current.GetInstance<IDiContainer>();
         var logger = container.GetInstance<IClassLoggerService>().GetClassLogger(GetType());
         logger.Info("Navigation wird eingeleitet.");
         var regionManager = container.GetInstance<IRegionManager>();
         var view = container.TryResolve<ContentView>();
         var viewModel = container.TryResolve<ContentViewModel>();

         //regionManager.RequestNavigate(RegionNames.CONTENT_REGION, new Uri(typeof (ContentView).FullName, UriKind.Relative),
         //   r => container.GetEvent<StatusbarStateChangedEvent>().Publish(@"View from 'ClickableButtons' loaded!"));
         var myParams = new NavigationParameters();
         myParams.Add("Key1", new MySharedObject("Hallo Welt!"));
         regionManager.RequestNavigate(RegionNames.CONTENT_REGION, new Uri(typeof (ContentView).FullName, UriKind.Relative), myParams);
      }

      public event EventHandler CanExecuteChanged;
   }
}