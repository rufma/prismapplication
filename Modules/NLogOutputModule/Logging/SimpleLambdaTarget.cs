﻿using System;
using NLog;
using NLog.Targets;

namespace de.webducer.dotnet.PrismApplication.NLogOutputModule.Logging {
   [Target("SimplePropertyLogging")]
   public class SimpleLambdaTarget : TargetWithLayout {
      private readonly Action<string> _logAction;

      public SimpleLambdaTarget(Action<string> logAction) {
         _logAction = logAction;
      }

      protected override void Write(LogEventInfo logEvent) {
         if (_logAction != null) {
            var logMessage = Layout.Render(logEvent);
            _logAction(logMessage);
         }
      }
   }
}